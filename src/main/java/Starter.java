import custom_tree.CustomTree;

import java.util.LinkedList;
import java.util.TreeMap;

public class Starter {
    public static void main(String[] args) {
        CustomTree<Integer, String> tree = new CustomTree<>();
        for (int i = 1; i < 17; i++) {
            tree.put(i, "value" + i);
        }
        System.out.println("List size is " + tree.getSize());
        printHeapLevels(tree);
        System.out.println("Depth now equals " + tree.getSize());
        System.out.println(tree.get(4));
        System.out.println();
        System.out.println("After removing 4");
        tree.remove(4);
        printHeapLevels(tree);
    }

    private static void printHeapLevels(CustomTree<Integer, String> tree){
        TreeMap<Integer, LinkedList<String>> treeLevels = tree.treeWalker();
        treeLevels.forEach((k, v) -> {
            System.out.print("Lvl = " + k + "; Nodes: ");
            v.forEach((e) -> System.out.print(e + ", "));
            System.out.println();
        });
    }
}
