package custom_tree;

import java.util.*;

public class CustomTree<K extends Comparable, V extends Comparable> {
    private int size;
    private Node<K, V> root;

    private final static class Node<K extends Comparable, V extends Comparable> {
        K key;
        V value;
        int treeLvl;
        Node<K, V> parent;
        Node<K, V> leftChild;
        Node<K, V> rightChild;
        boolean availableToAddLeftChild = true;
        boolean availableToAddRightChild = true;

        Node(K key, V value, Node<K, V> parent, CustomTree<K, V> customTree){
            this.key = key;
            this.value = value;
            this.parent = parent;
            customTree.size++;
            if (this.parent != null){
                this.treeLvl = this.parent.treeLvl + 1;
            } else {
                treeLvl = 1;
            }
        }

        void addLeftChild(Node<K, V> node){
            this.leftChild = Objects.requireNonNull(node);
            this.availableToAddLeftChild = false;
        }

        void addRightChild(Node<K, V> node){
            this.rightChild = Objects.requireNonNull(node);
            this.availableToAddRightChild = false;
        }

        boolean isAvailableToAddChildren(){
            return this.availableToAddLeftChild || this.availableToAddRightChild;
        }

        void deleteLeftChild(){
            this.leftChild = null;
            this.availableToAddLeftChild = true;
        }

        void deleteRightChild(){
            this.rightChild = null;
            this.availableToAddRightChild = true;
        }

        boolean isLeaf(){
            return this.availableToAddLeftChild && this.availableToAddRightChild;
        }

        void recalculateTreeLvl(){
            this.treeLvl = parent.treeLvl + 1;
        }
    }

    private void replaceValue(Node<K, V> node, V value){
        node.value = value;
    }

    private Node<K, V> containsCorrespondingNode(K givenKey){
        if (givenKey == null){
            throw new IllegalArgumentException();
        }
        Stack<Node<K, V>> stack = new Stack<>();
        stack.add(this.root);
        while (! stack.isEmpty()){
            Node<K, V> currentNode = stack.pop();
            if (currentNode.key.equals(givenKey)){
                return currentNode;
            } else {
                if (! currentNode.availableToAddLeftChild){
                    stack.add(currentNode.leftChild);
                }
                if (! currentNode.availableToAddRightChild){
                    stack.add(currentNode.rightChild);
                }
            }
        }
        return null;
    }

    private void putNewNode(K key, V value){
        Deque<Node<K, V>> deque = new LinkedList<>();
        deque.addLast(this.root);
        while (! deque.isEmpty()){
            Node<K, V> currentNode = deque.removeFirst();
            if (currentNode.isAvailableToAddChildren()){
                Node<K, V> child = new Node<>(key, value, currentNode, this);
                if (currentNode.availableToAddLeftChild){
                    currentNode.addLeftChild(child);
                    return;
                }
                if (currentNode.availableToAddRightChild){
                    currentNode.addRightChild(child);
                    return;
                }
            } else {
                if (! currentNode.availableToAddLeftChild){
                    deque.addLast(currentNode.leftChild);
                }
                if (! currentNode.availableToAddRightChild){
                    deque.addLast(currentNode.rightChild);
                }
            }
        }
    }

    public int getSize(){
        return this.size;
    }

    public void put(K key, V value){
        if (key == null || value == null){
            throw  new IllegalArgumentException();
        }
        if (this.root == null){
            this.root = new Node<>(key, value, null, this);
            return;
        }
        Node<K, V> temp = this.containsCorrespondingNode(key);
        if (temp != null){
            this.replaceValue(temp, value);
        }
        this.putNewNode(key, value);
    }

    public V get(K key){
        Node<K, V> node = containsCorrespondingNode(key);
        if (node != null){
            return node.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    public TreeMap<Integer, LinkedList<String>> treeWalker(){
        TreeMap<Integer, LinkedList<String>> treeLevels = new TreeMap<>();
        Deque<Node<K, V>> deque = new LinkedList<>();
        deque.addLast(this.root);
        while (! deque.isEmpty()){
            Node<K, V> currentNode = deque.removeFirst();
            if (! currentNode.availableToAddLeftChild){
                deque.addLast(currentNode.leftChild);
            }
            if (! currentNode.availableToAddRightChild){
                deque.addLast(currentNode.rightChild);
            }
            if (! treeLevels.containsKey(currentNode.treeLvl)){
                treeLevels.put(currentNode.treeLvl, new LinkedList<>());
            }
            K key;
            V value;
            String parentName;
            if (currentNode.parent == null){
                parentName = "-Root";
            } else {
                parentName = currentNode.parent.key.toString();
            }
            String nodeInfo = String.format("|key=%s, value=%s, p%s|",
                    currentNode.key, currentNode.value, parentName);
            treeLevels.get(currentNode.treeLvl).add(nodeInfo);
        }
        return treeLevels;
    }

    public void remove(K key){
        Node<K, V> node = this.containsCorrespondingNode(key);
        if (node == null){
            throw new NoSuchElementException();
        }
        Node<K, V> parentNode = node.parent;
        if (node.isLeaf()){
            if (parentNode.leftChild == node){
                parentNode.deleteLeftChild();
            }
            if (parentNode.rightChild == node){
                parentNode.deleteRightChild();
            }
            return;
        }
        Node<K, V> childNode = node.leftChild;
        if (node.isAvailableToAddChildren()){
            if (! node.availableToAddRightChild){
                childNode = node.rightChild;
            }
            childNode.parent = parentNode;
            if (parentNode.leftChild == node){
                this.replaceChild(parentNode, childNode);
            }
            if (parentNode.rightChild == node){
                this.replaceChild(parentNode, childNode);
            }
            return;
        }
        if (parentNode.leftChild == node){
            this.replaceChild(parentNode, childNode);
        }
        if (parentNode.rightChild == node){
            this.replaceChild(parentNode, childNode);
        }
        Node<K, V> rightChildNode = node.rightChild;
        this.putNewNode(rightChildNode.key, rightChildNode.value);
    }

    private void replaceChild(Node<K, V> parentNode, Node<K, V> childNode){
        parentNode.deleteLeftChild();
        childNode.parent = parentNode;
        parentNode.addLeftChild(childNode);
        childNode.recalculateTreeLvl();
    }
}
